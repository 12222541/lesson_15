#pragma once
#include <iostream>

#ifndef NUMBER_FUNCTIONS_H
#define NUMBER_FUNCTIONS_H

void findEvenNumbers(int maximum, bool even)
{
    if (even == true)
    {
        for (int n = 0; n <= maximum; n += 2)
        {
            std::cout << n << ", ";
        }
    }
    else
    {
        for (int n = 1; n <= maximum; n+=2)
        {
            std::cout << n << ", ";
        }
    }
    std::cout << "\n";
}
#endif